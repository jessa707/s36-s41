const User = require("../models/User")
const Course = require("../models/Course")
const auth = require('../auth')
const bcrypt = require("bcrypt")

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then(result => {
		if(result.length > 0){
			return true 
		} 

		return false
	})
}

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10) //hashSync function will 'hash' the password and turn it into random characters. The second argument serves as the amount of times that the password will be hashed. Note: The higher the number, the slower it is to 'unhash' which will affect application performance, and the lower the number the faster it will be.
	})

	return new_user.save().then((registered_user, error) => {
		if(error){
			return error 
		} 

		return 'User successfully registered!'
	})
}

module.exports.loginUser = (request_body) => {
	return User.findOne({email: request_body.email}).then(result => {
		if(result === null){
			return "The user doesn't exist."
		}

		// We can't use regular comparison to check if password is correct because the password of the existing user is hashed with bcrypt. We have to use bcrypt to de-hash the password and compare it to the password from the request body. The compareSync() function will return either true or false depending on if they match.
		const is_password_correct = bcrypt.compareSync(request_body.password, result.password)

		if(is_password_correct){
			return {
				// Using the createAccessToken function, we can generate a token using the user data after its password has been validated.
				accessToken: auth.createAccessToken(result.toObject())
			}
		}

		return 'The email and password combination is not correct!'
	})
}

// For getting user details from the token
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		return result
	})
}

// ========================= MINI ACTIVITY ===========================

module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id, {password: 0}).then((result) => {
		return result
	})
}

module.exports.enroll = async (data) => {

let is_user_updated = await User.findById(data.userId).then((user) => {
	user.enrollments.push({
		courseId: data.courseId
	})

	return user.save().then((update_user, error) => {
		if(error){
			return false
		}

		return true
	})
})

let is_course_updated = await Course.findById(data.courseId).then((course) => {
	course.enrollees.push({
		userId: data.userId
	})

	return course.save().then((update_course, error) => {
		if(error){
			return false
		}

		return true
	})
})


if(is_user_updated && is_course_updated){
	return {
		message: 'User enrollment is successful!'
	}
}


return {
	message: 'Something went wrong.'
}
}


module.exports.archieveCourse = (course_id, new_data) => {
	return Course.findByIdAndUpdate(course_id, {
		isActive: false
		}).then((archieve_course, error) => {
		if(error){
			return false
		}

		return {
			message: 'Course has been archieve successfully!'
		}
	})
}
